﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TextureSizes { 
    small,  // 32x32
    normal, // 64x64
    big,    // 128x128
    large,  // 256x256
}

/// <summary>
/// Замена инспектору для Dictionery...
/// </summary>
[System.Serializable]
public class TextureTypes
{
    public Vector2 textureSize = new Vector2 (32,32);
    public TextureSizes typeTexture = TextureSizes.small;
    public Color startColor = new Color(0, 0, 0, 1);
}

/// <summary>
/// Класс который создаёт, хранит, освобождает и отдаёт текстуры 
/// </summary>
public class TexturesController : MonoBehaviour {

    // подписываемся на смену сложности
    void OnEnable()
    {
        GameController.onChangeDifficulty += onChangeDifficulty;
    }

    void OnDisable()
    {
        GameController.onChangeDifficulty -= onChangeDifficulty;
    }

    //public List<Sprite> lS = new List<Sprite>();

    /// <summary>
    /// Список c размерами, типом и начальным ветом текстур
    /// </summary>
    public List<TextureTypes> textureTypesList = new List<TextureTypes>();

    /// <summary>
    /// Словарь текущих текстур
    /// </summary>
    Dictionary<TextureSizes, Sprite> _curSprites;

    /// <summary>
    /// Словарь будущих текстур
    /// </summary>
    Dictionary<TextureSizes, Sprite> _futureSprites;

    int _curDiff = 0;

    static TexturesController s_Instance = null;

    public static TexturesController instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Instance TexturesController already destroyed on application quit.");
                return null;
            }

            if (s_Instance == null)
            {
                Debug.Log("TexturesController is null");
                s_Instance = FindObjectOfType(typeof(TexturesController)) as TexturesController;

                // If it is still null, create a new instance
                if (s_Instance == null)
                {
                    GameObject obj = new GameObject("TexturesController");
                    DontDestroyOnLoad(obj);
                    s_Instance = obj.AddComponent(typeof(TexturesController)) as TexturesController;
                }

                // Problem during the creation, this should not happen
                if (s_Instance == null)
                {
                    Debug.LogError("Problem during the creation of " + typeof(TexturesController).ToString());
                }

                s_Instance.Init();
            }

            return s_Instance;
        }
    }

    void Awake()
    {
        if (s_Instance == null)
        {
            s_Instance = this as TexturesController;
            s_Instance.Init();
        }
        else
        {
            Debug.LogWarning("Destroy Copy of singltone TexturesController");
            Destroy(gameObject);
        }
    }

    private static bool applicationIsQuitting = false;
    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    void OnApplicationQuit()
    {
        applicationIsQuitting = true;
        s_Instance = null;
    }

    void Init()
    {
        // создаём текстуры
        GeneratePackTextures();       
        setFutureToCurrent();
    }

    /// <summary>
    /// Создание будущих текстур
    /// </summary>
    void GeneratePackTextures() {
        //обнуляем будущие спрайты
        _futureSprites = new Dictionary<TextureSizes, Sprite>();
        foreach (var tTexture in textureTypesList)
        {
            int X_size = (int) tTexture.textureSize.x;
            int Y_size = (int) tTexture.textureSize.y;
            if (X_size == 0 || Y_size == 0) {
                Debug.LogError("Размер спрайта не может быть равен 0");
            }

            // создаём текстуру нужного размера без mipmaps
            var texture = new Texture2D(X_size, Y_size, TextureFormat.ARGB32, false);

            // Цвет текстуры зависит от стартовго цвета и сложности
            var newColor = new Color(tTexture.startColor.r, tTexture.startColor.g, tTexture.startColor.b + 0.05f * _curDiff);
            // заполняем текстуру цветом
            FillTextureByColor(texture, new Color(1,1,1,0)); // чтобы стала прозрачной
            FillCircleTextureByColor(texture, X_size/2, Y_size/2, X_size / 2, newColor); // рисуем круг

            // создаём спрайт
            Sprite sprite = new Sprite();
            sprite = Sprite.Create(texture, new Rect(0, 0, X_size, Y_size), new Vector2(0.5f, 0.5f), X_size);
            Debug.Log("spriteAdd");
            //lS.Add(sprite);
            _futureSprites[tTexture.typeTexture] = sprite;
        }
    }

    /// <summary>
    /// Обновляем текущие спрайты на новые и начинаем создавать новые
    /// </summary>
    void setFutureToCurrent() {
        _curSprites = _futureSprites;
        GeneratePackTextures();
        Resources.UnloadUnusedAssets();
    }

    /// <summary>
    /// Заполинт полностью текстуру цветом 
    /// </summary>
    /// <param name="tex2d"></param>
    /// <param name="fillColor"></param>
    void FillTextureByColor(Texture2D tex2d, Color fillColor)
    {
        var fillColorArray = tex2d.GetPixels();

        for (var i = 0; i < fillColorArray.Length; ++i)
        {
            fillColorArray[i] = fillColor;
        }

        tex2d.SetPixels(fillColorArray);

        tex2d.Apply();
    }

    /// <summary>
    /// Рисует круг на текстуре заданного радиуса из заданных координат
    /// </summary>
    /// <param name="tex2d"></param>
    /// <param name="ox">центр х</param>
    /// <param name="oy">центр у</param>
    /// <param name="r"></param>
    /// <param name="fillColor"></param>
    public void FillCircleTextureByColor(Texture2D tex2d, int ox, int oy, int r, Color fillColor)
    {
        for (int x = -r; x < r; x++)
        {
            int height = (int)Mathf.Sqrt(r * r - x * x);

            for (int y = -height; y < height; y++)
                tex2d.SetPixel(x + ox, y + oy, fillColor);
        }
        tex2d.Apply();
    }

    void onChangeDifficulty(int curDif) {
        _curDiff = curDif;
        setFutureToCurrent();
    }

    public Sprite GetMeSprite(TextureSizes tSize) {
        if (!_curSprites.ContainsKey(tSize))
        {
            Debug.LogError("Не найден спрайт размера" + tSize.ToString());

        }
        return _curSprites[tSize];
    }

}
