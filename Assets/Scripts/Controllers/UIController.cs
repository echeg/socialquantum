﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Класс который отвечает за показ разных панелей UI
/// </summary>
public class UIController : MonoBehaviour {

    public delegate void ShowPanelHandler(UIPanels Panel);
    /// <summary>
    /// Событие отвечающие за показ заданной панели. Все панели подписаны на UIController
    /// когда происходит активация(ShowUIPanel), во все панели приходит сигнал что нужно активировать панель с именем Panel
    /// </summary>
    public static event ShowPanelHandler onShowPanel;

    static UIController s_Instance = null;

    public static UIController instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Instance UIController already destroyed on application quit.");
                return null;
            }

            if (s_Instance == null)
            {
                Debug.Log("UIController is null");
                s_Instance = FindObjectOfType(typeof(UIController)) as UIController;

                // If it is still null, create a new instance
                if (s_Instance == null)
                {
                    GameObject obj = new GameObject("UIController");
                    DontDestroyOnLoad(obj);
                    s_Instance = obj.AddComponent(typeof(UIController)) as UIController;
                }

                // Problem during the creation, this should not happen
                if (s_Instance == null)
                {
                    Debug.LogError("Problem during the creation of " + typeof(UIController).ToString());
                }

                s_Instance.Init();
            }

            return s_Instance;
        }
    }

    void Awake()
    {
        if (s_Instance == null)
        {
            s_Instance = this as UIController;
            s_Instance.Init();
        }
        else
        {
            Debug.LogWarning("Destroy Copy of singltone UIController");
            Destroy(gameObject);
        }
    }

    private static bool applicationIsQuitting = false;
    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    void OnApplicationQuit()
    {
        applicationIsQuitting = true;
        s_Instance = null;
    }

    void Init() { 
    }

    /// <summary>
    /// Показывает заданную UI панель 
    /// </summary>
    /// <param name="Panel"></param>
    public void ShowUIPanel(UIPanels Panel) {
        if (onShowPanel != null)
            onShowPanel(Panel);  
    }
}
