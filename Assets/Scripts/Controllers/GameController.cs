﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Основной управляющий игрой класс.
/// </summary>
public class GameController : MonoBehaviour
{
    public delegate void onDifficultyHandler (int curDifficulty);
    /// <summary>
    /// Подписываем объекты на смену сложности
    /// </summary>
    public static event onDifficultyHandler onChangeDifficulty;

    /// <summary>
    /// Префаб круга
    /// </summary>
    public GameObject circlPrefab;

    /// <summary>
    /// Базовое время между спавнами кружков
    /// </summary>
    public float baseSpawnTime = 1.0f;

    /// <summary>
    /// Текущие время между спавнами кружков падает с каждой сложностью
    /// </summary>
    float _curSpawnTime = 1.0f;

    /// <summary>
    /// Время прошедшее с последнего спавна
    /// </summary>
    float _timeSpawnDelta = 0;


    int _currentScore = 0;
    /// <summary>
    /// Current points
    /// </summary>
    public int CurrentScore
    {
        get { return _currentScore; }
        set { ChangeScore(value); }
    }

    int _difficultyLevel = 0;
    /// <summary>
    /// Текущая сложность уровня
    /// </summary>
    public int difficultyLevel { get { return _difficultyLevel; } }

    /// <summary>
    /// От чего растёт сложность уровня от времени или от очков
    /// </summary>
    public DifficultyType difficultyType = DifficultyType.ByTime;

    /// <summary>
    /// Кол-во очков через которое происходит увеличение сложности
    /// </summary>
    public int incrDiffEveryPoints = 1000;

    /// <summary>
    /// Время через которое происходит увеличение сложности
    /// </summary>
    public float incrDiffEverySeconds = 10.0f;

    /// <summary>
    /// Игра на паузе? По умолчанию да
    /// </summary>
    bool _gameisPause = true;

    // блок для тестирования утечек памяти - нужно ограничить кол-во объектов в сцене 
    // иначе просто всё время их будет становится больше и не понять куда уходит память
    /// <summary>
    /// Маквсимальное кол-во кружков
    /// </summary>
    public int maxCirclesInScene = 30;
    /// <summary>
    /// Текущие кол-во кружков в сцене, паблик для теста
    /// </summary>
    public int curCirclesInScene = 0;

    static GameController s_Instance = null;

    public static GameController instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Instance GameController already destroyed on application quit.");
                return null;
            }

            if (s_Instance == null)
            {
                Debug.Log("GameController is null");
                s_Instance = FindObjectOfType(typeof(GameController)) as GameController;

                // If it is still null, create a new instance
                if (s_Instance == null)
                {
                    GameObject obj = new GameObject("GameController");
                    DontDestroyOnLoad(obj);
                    s_Instance = obj.AddComponent(typeof(GameController)) as GameController;
                }

                // Problem during the creation, this should not happen
                if (s_Instance == null)
                {
                    Debug.LogError("Problem during the creation of " + typeof(GameController).ToString());
                }

                s_Instance.Init();
            }

            return s_Instance;
        }
    }

    void Awake()
    {
        if (s_Instance == null)
        {
            s_Instance = this as GameController;
            s_Instance.Init();
        }
        else
        {
            Debug.LogWarning("Destroy Copy of singltone GameController");
            Destroy(gameObject);
        }
    }

    private static bool applicationIsQuitting = false;
    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    void OnApplicationQuit()
    {
        applicationIsQuitting = true;
        s_Instance = null;
    }


    void Init()
    {
        // устанавливаем текущим базовый спавн
        _curSpawnTime = baseSpawnTime;
        // Игра в самом начале на паузе нужно установить Time.timeScale в 0 чтобы не шёл таймер
        // предполагается что GUI анимаций мы будем использовать timeScale независимые методы
        Time.timeScale = 0;
    }
    

    void Update() {

        if (_gameisPause) { 
            // если игра на паузе ничего не делаем
            return;
        }

        _timeSpawnDelta += Time.deltaTime;
        
        // проверяем достаточно ли прошло времени после спавна.
        if (_timeSpawnDelta >= _curSpawnTime && curCirclesInScene<maxCirclesInScene) {
            _timeSpawnDelta = 0;
            Instantiate(circlPrefab, new Vector3(-1000, -1000, 0), Quaternion.identity);
        }

        // Если сложность меняется от воемени то проверяем не пора ли сменить сложность
        if (difficultyType == DifficultyType.ByTime)
        {
            CheckComplexity();
        }
    }

    /// <summary>
    /// Меняет очки, если нужно меняет сложность
    /// </summary>
    /// <param name="newScore"></param>
    /// <returns></returns>
    void ChangeScore(int newScore)
    {
        // записали новые очки
        _currentScore = newScore;

        // если сложность меняется от очков то проверяем не пора ли сменить сложность
        if (difficultyType == DifficultyType.ByScore)
        {
            CheckComplexity();
        }
    }

    /// <summary>
    /// Проверка и изменение сложности
    /// </summary>
    void CheckComplexity() {

        int tempDiff = _difficultyLevel; 
        switch (difficultyType) {
            case (DifficultyType.ByScore):
                // увелечение сложности за очки
                tempDiff = _currentScore / incrDiffEveryPoints;              
                break;

            case (DifficultyType.ByTime):
                // увелечение сложности за время
                tempDiff = (int) (Time.time / incrDiffEverySeconds);
                break;
        }

        if (_difficultyLevel != tempDiff) {
            _difficultyLevel = tempDiff;
            
            // изменяем spwan time за каждую сложность ускоряем спавн на 10%
            _curSpawnTime = Mathf.Pow(0.9f, _difficultyLevel) * baseSpawnTime;

            // рассказываем о смене сложности всем заинтересованным
            if (onChangeDifficulty != null)
            {
                onChangeDifficulty(_difficultyLevel);
            }
        }

    }

    /// <summary>
    /// Метод устанавливает паузу в игре
    /// </summary>
    /// <param name="pause"></param>
    public void SetPause(bool pause)
    {
        if (pause)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
        _gameisPause = pause;
    }
}
