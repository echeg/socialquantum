﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class bundleObj {
    public string nameBundle;
    public BundleTypes typeBundle;
}

/// <summary>
/// Класс который отвечает за закачку бандов
/// </summary>
public class BundleController : MonoBehaviour {

    static BundleController s_Instance = null;
    public delegate void onDownloadHandler(int Curbundle, int AllBundles);
    /// <summary>
    /// События для того чтобы сообщить заинтересованным объектам что Curbundle из AllBundles загружено
    /// </summary>
    public static event onDownloadHandler onBundlesDownload;

    /// <summary>
    /// Сервер по умолчанию на котором лежат бандлы
    /// </summary>
    public string budnleServer = "http://omegadreams.com/bundle/";

    /// <summary>
    /// Список имён бандлов, которые нужны в этой сцене
    /// </summary>
    public List<bundleObj> nameBundleNeeds = new List<bundleObj>();

    // в данный момент скачено бандлов
    int _bundlesDownload;
    // всего бандлов
    int _allBundles;
    /// <summary>
    /// Очищать ли кеш при старте
    /// </summary>
    public bool cleanCacheOnStart = true;

    public static BundleController instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Instance BundleController already destroyed on application quit.");
                return null;
            }

            if (s_Instance == null)
            {
                Debug.Log("BundleController is null");
                s_Instance = FindObjectOfType(typeof(BundleController)) as BundleController;

                // If it is still null, create a new instance
                if (s_Instance == null)
                {
                    GameObject obj = new GameObject("BundleController");
                    DontDestroyOnLoad(obj);
                    s_Instance = obj.AddComponent(typeof(BundleController)) as BundleController;
                }

                // Problem during the creation, this should not happen
                if (s_Instance == null)
                {
                    Debug.LogError("Problem during the creation of " + typeof(BundleController).ToString());
                }

                s_Instance.Init();
            }

            return s_Instance;
        }
    }

    void Awake()
    {
        if (s_Instance == null)
        {
            s_Instance = this as BundleController;
            s_Instance.Init();
        }
        else
        {
            Debug.LogWarning("Destroy Copy of singltone BundleController");
            Destroy(gameObject);
        }
    }

    private static bool applicationIsQuitting = false;
    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    void OnApplicationQuit()
    {
        applicationIsQuitting = true;
        s_Instance = null;
    }


    void Init()
    {
        // очищаем кеш
        if (cleanCacheOnStart)
        {
            Caching.CleanCache();
        }

        // указываем сколько у нас всего бандлов
        _allBundles = nameBundleNeeds.Count;

        //Начинаем загружать бандлы
        foreach (var bundleObj in nameBundleNeeds)
        {

            DownloadBundles(bundleObj.nameBundle, bundleObj.typeBundle);
        }
    }


    void Start() {
        //оповещаем о кол-ве бандлов
        if (onBundlesDownload != null)
        {
            onBundlesDownload(0, _allBundles);
        }
    }

    void DownloadBundles(string bundleName, BundleTypes tBundle)
    {
        //создаём строку url с которой качать бандл
        string platform;
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            platform = "standalone/";
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            platform = "android/";
        }
        else 
        {
            platform = "standalone/";
        }
        var urlStr = budnleServer + platform + bundleName + ".unity3d";

        StartCoroutine(DownloadBundle(urlStr, bundleName, tBundle));
    }

    IEnumerator DownloadBundle(string BundleUrl, string bundleName, BundleTypes tBundle)
    {
        AssetBundleLoader assetBundleLoader = new AssetBundleLoader();
        yield return StartCoroutine(assetBundleLoader.LoadBundle<GameObject>(BundleUrl, 1, "", ""));
        if (assetBundleLoader.Obj != null)
        {
            Debug.Log("is load!");
            //GameObject ObjInstance = (GameObject)Instantiate(assetBundleLoader.Obj);
            // если тип для Instantiate то просто создаём, например фоновая музыка
            if (tBundle == BundleTypes.toInstantiate)
            {
                Instantiate(assetBundleLoader.Obj);
            } 
            else if (tBundle == BundleTypes.toPoolManager)
            {
                //PoolManager.objects.
                PoolManager.objects[bundleName]=assetBundleLoader.Obj;
            }
        }
        else
        {
            // когда нибудь здесь будет логировщик ошибок, который в свою очередь работает с GUI
            Debug.LogError("Not download bundle");
        }

        _bundlesDownload+=1;
        //оповещаем что так или иначе бандл скачен.
        if (onBundlesDownload != null) {
            onBundlesDownload(_bundlesDownload, _allBundles);
        }
    }



}
