﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Реализациия простейшей локализации, для того чтобы можно было потом легко сделать рефакторинг
/// </summary>
public static class Localization {

    /// <summary>
    /// Вложенный словарь в котором мы храним все строки для разных языков
    /// </summary>
    static Dictionary<string, Dictionary<string, string>> allText = new Dictionary<string, Dictionary<string, string>>()
    {
        {"English", new Dictionary<string, string>()
            {
                {"scoreLabel","Score: "},
                {"timerLabel","Timer: "},
                {"diffLabel","Level: "},
                {"Loading assets...","Loading assets..."},
            } 
        },
    };

    /// <summary>
    /// Локализует строку
    /// </summary>
    /// <param name="inStr"></param>
    /// <returns></returns>
    public static string Localize(string inStr) {
        if (allText["English"].ContainsKey(inStr))
        {
            return allText["English"][inStr];
        }
        else {
            Debug.LogWarning("Строка " + inStr+" не локализована");
            return inStr;
        }
    }
}
