﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Класс который позвоялет двигать объект к которму прикреплён
/// </summary>
public class Mover : MonoBehaviour {

    public float speed = 1;

    /// <summary>
    /// Начинаем движение объекта к выбранной границе уровня
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="speed">При 1 скорости объект преодолет растояние за 1с</param>
    public void StartLinearMove(MoveDirection direction, float speed)
    {
        if (direction == MoveDirection.Down)
        {
            StartLinearMove(new Vector3(transform.position.x, -Camera.main.camera.orthographicSize, transform.position.z), speed);
        }
    }

    /// <summary>
    /// Начинаем движение объекта в заданные координаты
    /// </summary>
    /// <param name="toPosition"></param>
    /// <param name="speed">При 1 скорости объект преодолет растояние за 1с</param>
	public void StartLinearMove (Vector3 toPosition, float speed) {
        this.speed = speed;
        StartCoroutine(MoveObject(transform.position, toPosition));
	}

    IEnumerator MoveObject(Vector3 startPos, Vector3 endPos)
    {

        while ((startPos-endPos).magnitude>0.01f)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, endPos, step);

            yield return new WaitForEndOfFrame();
        }

        transform.position = endPos;
    }



    /*
    IEnumerator MoveObject(Vector3 startPos, Vector3 endPos)
    {
        float progress = 0.0f;

        while (progress < 1.0f)
        {
            transform.position = Vector3.Lerp(startPos, endPos, progress);

            yield return new WaitForEndOfFrame();
            progress += Time.deltaTime * speed;
        }

        transform.position = endPos;
    }*/

}
