﻿using UnityEngine;
using System.Collections;

public enum ColorOrTexture {
    useColor,
    useTextures,
}
/// <summary>
/// Базовый класс для объектов, которые "могут падать сверху экрана"
/// </summary>
public class BaseObj : MonoBehaviour, IClick {

    /// <summary>
    /// Показывать просто смещением цвета или использовать генератор текстур
    /// </summary>
    public ColorOrTexture typeView = ColorOrTexture.useColor;

    /// <summary>
    /// Минимальный размер кружка
    /// </summary>
    public int minSize = 50;
    
    /// <summary>
    /// Максимальный размер кружка
    /// </summary>
    public int maxSize = 256;


    /// <summary>
    /// Базовая скорость
    /// </summary>
    public float baseSpeed = 100;


    /// <summary>
    /// Размер объекта
    /// </summary>
    int _curSize = 256;

    /// <summary>
    /// Размер объекта
    /// </summary>
    float _curSpeed = 100;

    /// <summary>
    /// Размер объекта
    /// </summary>
    int _curPoints = 10;

    /// <summary>
    /// Ссылка на спрайт
    /// </summary>
    SpriteRenderer _linkToSprite;

    /// <summary>
    /// Ссылка на Mover
    /// </summary>
    Mover _linkToMover;

    void OnEnable()
    {
        GameController.onChangeDifficulty += onChangeDifficulty;
    }

    void OnDisable()
    {
        GameController.onChangeDifficulty -= onChangeDifficulty;
    }

    /// <summary>
    /// Метод который происходит во время смены сложности
    /// </summary>
    /// <param name="newDifficulty">Новая сложность</param>
    void onChangeDifficulty(int newDifficulty) {
        // меняем скорость, цвет\текстуру
        SetSpeedAndView();
    }

    /// <param name="isPress"></param>
    public void OnPress(bool isPress){
        GameController.instance.CurrentScore += _curPoints;
        KillMe();
    }
    
    /// <summary>
    /// Объект достиг граници уровня
    /// </summary>
    public void BorderReached()
    {
        KillMe();
    }

    /// <summary>
    /// Объект заканчивает свою жизнь. Нужно будет сделать чтобы возращался в пулл объектов
    /// </summary>
    void KillMe(){
        //запускаем эффект
        string key = "simpleEffect";
        if (PoolManager.objects.ContainsKey(key))
        {
            var objI = PoolManager.objects[key];
            GameObject ObjInstance = (GameObject)Instantiate(objI);
            ObjInstance.transform.position = transform.position;
            // запускаем эффект
            ObjInstance.GetComponent<SimpleEffect>().PlayEffect();
        }

        GameController.instance.curCirclesInScene -= 1;
        Destroy(gameObject);
    }

    void Awake() {
        // говорим контроллеру что объектов стало больше - нужно для теста утечки памяти
        GameController.instance.curCirclesInScene += 1;

        // кешируем ссылку на спрайт
        _linkToSprite = gameObject.GetComponent<SpriteRenderer>();
        if (_linkToSprite == null) {
            Debug.LogError("Спрайт не найден");
        }
        // кешируем ссылку на Mover
        _linkToMover = gameObject.GetComponent<Mover>();
        if (_linkToMover == null)
        {
            Debug.LogError("Mover не найден");
        }

        // устанавливаем размер стартовую позицию и очки
        RandomStart();

        //Устанавливаем цвет\текстуру и скорсть от сложности и размера
        SetSpeedAndView();

        // запускаем движение
        _linkToMover.StartLinearMove(MoveDirection.Down, _curSpeed);
    }

    /// <summary>
    /// Устанавливаем случайный размер и случайную стартовую позицию.
    /// </summary>
    void RandomStart() {
        // определяем и устанавливаем случайный размер
        _curSize = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(_curSize, _curSize, 1);

        // от размера считам кол-во очков базово 10 + бонус от размера 
        _curPoints = (int)((float)maxSize / (float)_curSize * 10) + 10;

        var vertExtent = Camera.main.camera.orthographicSize;
        var horzExtent = vertExtent * Screen.width / Screen.height;

        // определяем граници где может появится объект
        float minX = -horzExtent + _curSize / 2 + 1;
        float maxX = horzExtent - _curSize / 2 - 1;

        // выбираем случайную координату
        float curX = Random.Range(minX, maxX);

        // устанавливаем позицию над верхней границей экрана
        transform.position = new Vector3(curX, vertExtent + _curSize / 2, 0);
    }

    /// <summary>
    /// В зависимости от размера и сложности устанавливаем цвет и скорость
    /// </summary>
    void SetSpeedAndView() { 
        int curDif = GameController.instance.difficultyLevel;
        // устанавливаем первичную скорость для этого берём минимальную скорость и за каждую сложность увеличиваем её на 20%
        _curSpeed = baseSpeed * (curDif / 5 + 1);
        // передаём скорость в Mover чтобы при смене сложности существующие объекты в сцене ускорились
        _linkToMover.speed = _curSpeed;

        if (typeView == ColorOrTexture.useColor)
        {
            // устанавливаем цвет в зависимости от размера
            float modifColor = 1.0f / (float)(maxSize - minSize);
            float tColor = (float)(_curSize - minSize) * modifColor;

            // самый большой будет чёрным, самый маленьки красным на сложности 0
            // за каждую сложность меняем добавляем синего
            _linkToSprite.color = new Color(1f - tColor, 0, 0.1f * (float)curDif, 1);
        }
        else if (typeView == ColorOrTexture.useTextures)
        {
            //transform.localScale = new Vector3(1,1,1)
            // определяем размер нужной текстуры
            TextureSizes tSize = TextureSizes.small;
            if (_curSize >= maxSize * 0.9f) {
                tSize = TextureSizes.large;
            }
            else if (_curSize >= maxSize * 0.6f)
            {
                tSize = TextureSizes.big;
            }
            else if (_curSize >= maxSize * 0.3f)
            {
                tSize = TextureSizes.normal;
            } 

            var newSprite = TexturesController.instance.GetMeSprite(tSize);
            _linkToSprite.sprite = newSprite;
        }
    }


}
