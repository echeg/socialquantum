﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Простейший запускатель эффекта
/// </summary>
public class SimpleEffect : MonoBehaviour {

    /// <summary>
    /// Ссылка на аудио
    /// </summary>
    public AudioSource audioSource;
    /// <summary>
    /// Ссылка на систему частиц
    /// </summary>
    public ParticleSystem particleSys;
    /// <summary>
    /// Время до уничтожения
    /// </summary>
    public float timeToDestroy = 1;


    public void PlayEffect() {
        audioSource.Play();
        particleSys.Play();
        StartCoroutine(waitToDestroy());
    }

    //вместо уничтожения объект должен возвращаться в пул
    IEnumerator waitToDestroy() {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }
}
