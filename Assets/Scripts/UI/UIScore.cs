﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Класс который показывает плавно изменение кол-ва очков
/// </summary>
public class UIScore : MonoBehaviour {

    /// <summary>
    /// Текущие кол-во очков, которое показывается на экране
    /// </summary>
    int _curScore = 0;

    /// <summary>
    /// Основонй текст
    /// </summary>
    public GUIText guiTextMain;
    /// <summary>
    /// Теневой текст
    /// </summary>
    public GUIText guiTextShadow;

    // Use this for initialization
    void Start()
    {
        if (guiTextMain == null || guiTextShadow == null)
        {
            Debug.LogError("Не указаны GUIText");
        }
        var tStr = Localization.Localize("scoreLabel") + 0;
        guiTextMain.text = tStr;
        guiTextShadow.text = tStr;
    }

    // Update is called once per frame
    void Update()
    {
        var _realScore = GameController.instance.CurrentScore;
        if (_curScore == _realScore) {
            return;
        }

        int modif = Mathf.Abs(_realScore - _curScore) / 20 + 1;
        if (_curScore > _realScore)
        {
            _curScore -= 1 * modif;
        }
        if (_curScore < _realScore)
        {
            _curScore += 1 * modif;
        }

        if (guiTextMain != null && guiTextShadow != null)
        {
            var tStr = Localization.Localize("scoreLabel") + _curScore;
            guiTextMain.text = tStr;
            guiTextShadow.text = tStr;
        }
    }
}
