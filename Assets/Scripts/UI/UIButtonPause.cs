﻿using UnityEngine;
using System.Collections;

public class UIButtonPause : UIButton {

    /// <summary>
    /// Устанавливаем паузу или наоборот снимаем
    /// </summary>
    public bool SetPause = true;
    public override void OnPress(bool isPressed)
    {
        base.OnPress(isPressed);
        if (isPressed)
        {
            
            //Debug.Log("isPressed " + isPressed + " " + gameObject);
            GameController.instance.SetPause(SetPause);
        } 
    }
}
