﻿using UnityEngine;
using System.Collections;

public class UITimer : MonoBehaviour {

    /// <summary>
    /// Основонй текст
    /// </summary>
    public GUIText guiTextMain;
    /// <summary>
    /// Теневой текст
    /// </summary>
    public GUIText guiTextShadow;

	// Use this for initialization
	void Start () {
        if (guiTextMain == null || guiTextShadow == null) {
            Debug.LogError("Не указаны GUIText");
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (guiTextMain != null && guiTextShadow != null) {
            var tStr = Localization.Localize("timerLabel") + Time.time.ToString("0.00");
            guiTextMain.text = tStr;
            guiTextShadow.text = tStr;
        }
	}
}
