﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Класс который переводит надпись в GUIText
/// </summary>
public class UILocalazGUIText : MonoBehaviour {

    public string localString = "";
	void Start () {
        var gt = gameObject.GetComponent<GUIText>();
        if (gt == null) {
            Debug.LogError("Не найден GUIText");
            return;
        }
        if (localString == "") localString = gt.text;
        gt.text = Localization.Localize(localString);
	}
	

}
