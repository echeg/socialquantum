﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Базовый класс для нопок меню
/// </summary>
public class UIButton : MonoBehaviour, IClick
{
    /// <summary>
    /// Какую панель показывать по клику. None если никакую
    /// </summary>
    public UIPanels ShowPanel = UIPanels.None;

    /// <summary>
    /// Метод срабатывающий по нажатию - для кнопок плох, для них нужно сделать
    /// другой который будет срабатывать на клик, а не на нажатие.
    /// </summary>
    /// <param name="isPressed"></param>
    public virtual void OnPress(bool isPressed)
    {
        if (isPressed)
        {
            if (ShowPanel != UIPanels.None)
            {
                UIController.instance.ShowUIPanel(ShowPanel);
            }
        }
    }	

}
