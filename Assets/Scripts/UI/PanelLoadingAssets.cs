﻿using UnityEngine;
using System.Collections;

public class PanelLoadingAssets : SimpleUIPanel {

    /// <summary>
    /// Ссылка на текст сколько из скольки бандлов загружено
    /// </summary>
    public GUIText textBundleLoad;
    /// <summary>
    /// Какая панель будет следу после загрузки всех бандлов
    /// </summary>
    public UIPanels nextUIPanel;


    public override void OnEnable()
    {
        base.OnEnable();
        BundleController.onBundlesDownload += onBundlesDownload;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        BundleController.onBundlesDownload -= onBundlesDownload;
    }

    public void onBundlesDownload(int curBudnle, int allBundle)
    {
        // пишем сколько из скольки бандлов загружено на данный момент
        textBundleLoad.text = "("+curBudnle.ToString() + "/" + allBundle.ToString()+")";
        // если все бандлы загружены то можно перейти на след сцену
        if (curBudnle == allBundle)
        {
            UIController.instance.ShowUIPanel(nextUIPanel);
        }
    }


}
