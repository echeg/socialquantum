﻿using UnityEngine;
using System.Collections;

public class UIDifficulty : MonoBehaviour {

    /// <summary>
    /// Основонй текст
    /// </summary>
    public GUIText guiTextMain;
    /// <summary>
    /// Теневой текст
    /// </summary>
    public GUIText guiTextShadow;

    void OnEnable()
    {
        GameController.onChangeDifficulty += onChangeDifficulty;
    }

    void OnDisable()
    {
        GameController.onChangeDifficulty -= onChangeDifficulty;
    }

    // Use this for initialization
    void Start()
    {
        if (guiTextMain == null || guiTextShadow == null)
        {
            Debug.LogError("Не указаны GUIText");
        }
        var tStr = Localization.Localize("diffLabel") + 0;
        guiTextMain.text = tStr;
        guiTextShadow.text = tStr;
    }

    void onChangeDifficulty(int curDiff) {
        var tStr = Localization.Localize("diffLabel") + curDiff;
        guiTextMain.text = tStr;
        guiTextShadow.text = tStr;
    }
}
