﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class UIAnchor : MonoBehaviour {

    /// <summary>
    /// Камера на основе которой определяем смещение
    /// </summary>
    public Camera curCamera;

    /// <summary>
    /// Якорь к одной из сторон
    /// </summary>
    public Anchors anchor;

    /// <summary>
    /// Отступ
    /// </summary>
    public Vector2 pixelOffset = Vector2.zero;

    /// <summary>
    /// Отступ в экранах
    /// </summary>
    public Vector2 relativeOffset = Vector2.zero;

    /// <summary>
    /// Применить сейчасже
    /// </summary>
    public bool immediately = false;

    /// <summary>
    /// Применить только на старте или в каждый update
    /// </summary>
    public bool onlyOnce = false;

    void Awake() {
        if (curCamera == null) {
            curCamera = Camera.main;
        }
        ApplyAnchor();
     }

    /// <summary>
    /// Пересчитываем Якорь от текущих данных
    /// </summary>
    public void ApplyAnchor() {
        Vector3 p = Vector3.zero; // новая позиция
        //ViewportToWorldPoint The bottom-left of the camera is (0,0); the top-right is (1,1).
        switch (anchor)
        {
            case Anchors.rightTop:
                p = curCamera.ViewportToWorldPoint(new Vector3(1 + relativeOffset.x, 1 + relativeOffset.y, curCamera.nearClipPlane));
                break;
            case Anchors.leftTop:
                p = curCamera.ViewportToWorldPoint(new Vector3(0 + relativeOffset.x, 1 + relativeOffset.y, curCamera.nearClipPlane));
                break;
            case Anchors.rightDown:
                p = curCamera.ViewportToWorldPoint(new Vector3(1 + relativeOffset.x, 0 + relativeOffset.y, curCamera.nearClipPlane));
                break;
            case Anchors.leftDown:
                p = curCamera.ViewportToWorldPoint(new Vector3(0 + relativeOffset.x, 0 + relativeOffset.y, curCamera.nearClipPlane));
                break;
            case Anchors.center:
                p = curCamera.ViewportToWorldPoint(new Vector3(0.5f + relativeOffset.x, 0.5f + relativeOffset.y, curCamera.nearClipPlane));
                break;
        }
        transform.position = new Vector3(Mathf.Floor(p.x), Mathf.Floor(p.y), 0) + (Vector3)pixelOffset;
    }

    void Update() {
        if (immediately || !onlyOnce) {
            immediately = false;
            ApplyAnchor();
        }
    }

  
}
