﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Самый простейшее растягивание спрайта на весь экран
/// </summary>
public class UIBGStretch : MonoBehaviour {

    /// <summary>
    /// Камера на основе которой устанавливаем размер
    /// </summary>
    public Camera curCamera;

    /// <summary>
    /// Линк на спрайт
    /// </summary>
    public SpriteRenderer _spriteBG;

    void Awake()
    {
        if (curCamera == null)
        {
            curCamera = Camera.main;
        }

        _spriteBG = gameObject.GetComponent<SpriteRenderer>();
        if (_spriteBG == null)
        {
            Debug.LogError("Не найден спрайт");
        }
        var vertExtent = Camera.main.camera.orthographicSize;
        var horzExtent = vertExtent * Screen.width / Screen.height;

        // граници спрайта на граници влияет localScale
        var xS = renderer.bounds.size.x;
        var yS = renderer.bounds.size.y;

        // реальный размер спрайта 
        var realX = xS / transform.localScale.x;
        var realY = yS / transform.localScale.y;

        //Debug.Log(renderer.bounds.size);
        transform.localScale = new Vector3(horzExtent / realX * 2, vertExtent / realY * 2, 1);

    }
	

}
