﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Базовая панель UI
/// </summary>
public class SimpleUIPanel : MonoBehaviour
{
    /// <summary>
    /// Тип панели
    /// </summary>
    public UIPanels Panel = UIPanels.Main;
    /// <summary>
    /// Активирована ли она по умолчанию или нет
    /// </summary>
    public bool isActive = false;
    /// <summary>
    /// Номер панели используется для оффсета, чтобы панели были равномерно расположены в редакторе
    /// </summary>
    public int numP = 0;

    public virtual void OnEnable()
    {
        UIController.onShowPanel += onActivate;
    }

    public virtual void OnDisable()
    {
        UIController.onShowPanel -= onActivate;
    }


    void Start()
    {
        // если панель по умолчанию должна быть активирована, то активируем
        if (isActive) ManagePanel(isActive);
    }

    /// <summary>
    /// Показывает панель PtoActive и скрывает все остальные активные
    /// </summary>
    /// <param name="PtoActive"></param>
    void onActivate(UIPanels PtoActive)
    {
        if (Panel != PtoActive)
        {
            // если активна то деактивировать - скрываем
            if (isActive)
            {
                ManagePanel(false);
                CutsomDeActivate();
            }
        }
        else
        {
            ManagePanel(true);
            isActive = true;
            CutsomActivate();
        }
    }

    /// <summary>
    /// Смещает все якоря в панели
    /// </summary>
    /// <param name="Active"></param>
    void ManagePanel(bool Active)
    {
        UIAnchor[] Anchors = gameObject.GetComponentsInChildren<UIAnchor>();
        foreach (UIAnchor Anchor in Anchors)
        {
            if (Active)
            {
                Anchor.relativeOffset = new Vector2(0, 0);
            }
            else
            {
                Anchor.relativeOffset = new Vector2(2 + 1 * numP, 0);
            }
            // Применяем с новыми параметрами
            Anchor.ApplyAnchor();
        } 
    }

    /// <summary>
    /// Метод для дополнительного кода при закрытии панели. Например отключение какой то анимации
    /// </summary>
    public virtual void CutsomDeActivate()
    {
    }

    /// <summary>
    /// Метод для дополнительного кода при показе панели. Например включение какой то анимации
    /// </summary>
    public virtual void CutsomActivate()
    {
    }

}
