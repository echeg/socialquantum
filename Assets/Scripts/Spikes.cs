﻿using UnityEngine;
using System.Collections;

public class Spikes : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D coll)
    {
        //Debug.Log("collide");
        if (coll.gameObject.tag == "FallObj")
        {
            //Debug.Log("collide with");
            coll.gameObject.SendMessage("BorderReached");
        }
    }
}
