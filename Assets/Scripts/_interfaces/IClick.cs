﻿using UnityEngine;
using System.Collections;

interface IClick
{
    void OnPress(bool isDown);
}
