﻿using UnityEngine;
using System.Collections;


public class SmartCamera : MonoBehaviour {

    /// <summary>
    /// Камера из которой будет идти RayCast
    /// </summary>
    public Camera rayCamera = null;

    /// <summary>
    /// Какие слои будут реагировать на клик с этой камеры
    /// </summary>
    public LayerMask eventLayersMask = -1;  // -1 все

    /// <summary>
    /// Какие слои будут реагировать на клик с этой камеры
    /// </summary>
    public RaycastType rayType = RaycastType.Unity2D;

    /// <summary>
    /// Разрешено ли использование мультитач
    /// </summary>
    public bool allowMultiTouch = true;

    /// <summary>
    /// Место куда попал луч
    /// </summary>
    RaycastHit rayHit;

    /// <summary>
    /// GO в который попал луч
    /// </summary>
    GameObject goHit;

    /// <summary>
    /// Инициализация - подключение к камере
    /// </summary>
    void Awake() { 
        // проверяем указана ли камера вручную
        if (rayCamera == null) {
            // пытаемся найти камеру в этом геймобъекте
            var tempCamera = gameObject.GetComponent<Camera>();
            if (tempCamera == null)
            {
                Debug.LogError("Camera not found");
            }
            else {
                rayCamera = tempCamera;
            }
        }
    }

    /// <summary>
	/// Проверка инпута и его обработка
	/// </summary>
    void Update()
    {
        // обрабатываем прикосновения
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (i > 0 && !allowMultiTouch)
            {
                // мультитач запрещён
                break;
            }
            Touch touch = Input.GetTouch(i);
            if (findClick(touch.position))
            {
                Notify(goHit, "OnPress", true);
            }
        }
        // проверяем нажата ли левая клавиша мыши
        if (Input.GetMouseButtonDown(0))
        {
            if (findClick(Input.mousePosition))
            {
                Notify(goHit, "OnPress", true);
            }
        }
    }

    /// <summary>
    /// Метод который находит куда приходится нажатие. В случе 3D использует Raycast в случае 2D использует OverlapPoint
    /// </summary>
    /// <param name="Pos">Координаты клика</param>
    /// <returns></returns>
    bool findClick(Vector3 Pos)
    {
        
        Vector3 viewPoint = rayCamera.ScreenToViewportPoint(Pos); // The bottom-left of the camera is (0,0); the top-right is (1,1)
        if (float.IsNaN(viewPoint.x) || float.IsNaN(viewPoint.y)) 
            return false;

        // если не в пределах камеры
        if (viewPoint.x < 0f || viewPoint.x > 1f || viewPoint.y < 0f || viewPoint.y > 1f) 
            return false;

        if (rayType == RaycastType.Unity3D)
        {
            // создаём луч от камеры в координаты
            Ray ray = rayCamera.ScreenPointToRay(Pos);

            // смотрим произошло ли столкновение
            if (Physics.Raycast(ray, out rayHit, Mathf.Infinity, eventLayersMask.value)) {
                goHit = rayHit.transform.gameObject;
                return true;
            } 
        }

        if (rayType == RaycastType.Unity2D)
        {
            // находим координаты нажатия в мире
            Vector3 worldPoint = rayCamera.ScreenToWorldPoint(Pos);
            // смотрим есть ли пересечения с коллайдером
            Collider2D c2d = Physics2D.OverlapPoint(worldPoint, eventLayersMask);
            if (c2d)
            {
                goHit = c2d.gameObject;
                return true;
            }
        }
        return false;
    }


    /// <summary>
    /// SendMessage в объект 
    /// </summary>
    static public void Notify(GameObject go, string funcName, object obj)
    {
        if (go != null)
        {
            go.SendMessage(funcName, obj, SendMessageOptions.DontRequireReceiver);
        }
    }
}
