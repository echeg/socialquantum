﻿public enum UIPanels
{
    None,       // Ни какая
    Main,       // Пре игровой UI
    Loading,    // UI загрузки
    Error,      // UI ошибки
    Game,       // UI во время игры
    PauseGame,  // UI во время паузы в игре
}