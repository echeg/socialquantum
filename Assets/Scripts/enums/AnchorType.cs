﻿public enum Anchors
{
    leftTop,
    rightTop,
    leftDown,
    rightDown,
    center,
}
