﻿public enum BundleTypes
{
    toInstantiate, // такие объекты просто будут сразу созданы
    toPoolManager, // такие объекты будут посланы в пул менеджер
}
