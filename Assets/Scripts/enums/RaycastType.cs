﻿public enum RaycastType
{
    Unity3D,		    // Physics.Raycast
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_1 && !UNITY_4_2
    Unity2D,	        // Physics2D.OverlapPoint
#endif
}