// C# Example
// Builds an asset bundle from the selected objects in the project view.
// Once compiled go to "Menu" -> "Assets" and select one of the choices
// to build the Asset Bundle

using UnityEngine;
using UnityEditor;
public class ExportAssetBundles
{
    [MenuItem("Assets/Build AssetBundle From Selection - Track dependencies StandAlone")]
    static void ExportResourceStandalone() {
        ExportResource(BuildTarget.StandaloneWindows);
    }
    [MenuItem("Assets/Build AssetBundle From Selection - Track dependencies Android")]
    static void ExportResourceAndroid()
    {
        ExportResource(BuildTarget.Android);
    }

    static void ExportResource(BuildTarget targetP)
    {
        // Bring up save panel
        string path = EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
            BuildPipeline.BuildAssetBundle(Selection.activeObject, selection, path, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, targetP);
            Selection.objects = selection;
        }
    }

    [MenuItem("Assets/Build AssetBundle From Selection - No dependency tracking StandAlone")]
    static void ExportResourceNoTrackStandalone()
    {
        ExportResourceNoTrack(BuildTarget.StandaloneWindows);
    }
    [MenuItem("Assets/Build AssetBundle From Selection - No dependency tracking Android")]
    static void ExportResourceNoTrackAndroid()
    {
        ExportResourceNoTrack(BuildTarget.Android);
    }


    static void ExportResourceNoTrack(BuildTarget targetP)
    {
        // Bring up save panel
        string path = EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            BuildPipeline.BuildAssetBundle(Selection.activeObject, Selection.objects, path, BuildAssetBundleOptions.CompleteAssets, targetP);
        }
    }

    [MenuItem("Assets/Build AssetBundle From Selection - No dependency tracking Basic")]
    static void ExportResourceNoTrackB()
    {
        // Bring up save panel
        string path = EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
            BuildPipeline.BuildAssetBundle(Selection.activeObject, selection, path);
        }
    }
}